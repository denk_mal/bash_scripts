#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#
# this is a msmtp helper script to help msmtp to work with the
# /etc/aliases files. The /etc/aliases file contains line like the following:
# cron: admin@company.invalid
# root: devel@company.invalid
# default: admin@company.invalid
# The default line contains the fallback email address for unknown users
#
# to activate it, rename the original /usr/bin/msmtp to /usr/bin/msmtp.bin
# and set a link from /usr/bin/msmtp to this file (or copy this file to
# /usr/bin/msmtp)

ALIASFILE="/etc/aliases"

####################### internals #########################

function trim() {
  local var="${*}"
  # remove leading whitespace characters
  var="${var#"${var%%[![:space:]]*}"}"
  # remove trailing whitespace characters
  var="${var%"${var##*[![:space:]]}"}"
  echo -n "${var}"
}

function get_alias_email() {
  declare -n arry=${1}
  replace_user=${2}

  if [ "${replace_user}" = "" ]; then
    # this is an error!
    echo "-------------"
    return
  fi

  # search recursively until a real email address is found
  # or no
  while [[ ! "${replace_user}" =~ "@" ]]; do
    if [[ ${arry[${replace_user}]} ]]; then
      replace_user=${arry[${replace_user}]}
    else
      break
    fi
  done

  if [[ "${replace_user}" =~ "@" ]]; then
    echo "${replace_user}";
    return;
  fi

  echo "${arry[default]:=${replace_user}}"
}

declare -A aliases

# read aliasfile and parse into array
test -f "${ALIASFILE}" && readarray -t lines < "${ALIASFILE}"
for line in "${lines[@]}"; do
  if [ "${line}" = "" ] || [ "${line:0:1}" = "#" ]; then
    continue
  fi
  aliases[$(trim "${line%%:*}")]=$(trim "${line#*:}")
done

# cleanup aliases array
for alias_user in "${!aliases[@]}"; do
  aliases["${alias_user}"]=$(get_alias_email aliases "${alias_user}")
done

is_in_header=true
input=""
while read -r -t0.1 line; do
  if [ ${is_in_header} = true ]; then
    header_key=${line%%:*}    # get key (before ':')
    header_value=${line#*:}   # get value (after ':')
    case "${header_key,,}" in # check lowercase!
      "from"|"to" )
        if [[ ! "${header_value}" =~ "@" ]]; then
          # line contains no '@' aka no email address
          # so try to search for
          mailuser=$(trim "${header_value%%(*}")
          realname=$(trim "${header_value#*(}")
          realname=$(trim "${realname%)*}")
          # get the email address for the mailuser or the default
          # email address as fallback if the mailuser is not found
          mailuser=${aliases[${mailuser}]:=${aliases[default]}}
          line="${header_key}: ${realname} <${mailuser}>"
        else
          # split header_value into mailuser (email adress) and
          # and realname (text before and after email address)
          mailuser=$(trim "${header_value#*<}")
          mailuser=$(trim "${mailuser%%>*}")
          prefix=$(trim "${header_value%%<*}")
          postfix=$(trim "${header_value##*>}")
          realname=$(trim "${prefix} ${postfix}")
          if [[ "${realname}" = *"("* ]]; then
            # if realname contains '(' and ')' use content
            # as realname
            realname=$(trim "${realname#*(}")
            realname=$(trim "${realname%)*}")
          fi
          if [ "${realname}" = "" ]; then
            #if realname is empty use username of email address
            realname=${mailuser%@*}
          fi
          line="${header_key}: ${realname} <${mailuser}>"
        fi
        ;;
      "" ) # header ends with an empty line
        is_in_header=false
        ;;
    esac
  fi
  input="${input}${line}""\n"
done < /dev/stdin

echo -e "${input}" | /usr/bin/msmtp.bin "$@"
