#!/usr/bin/env zsh
#
# https://gitlab.com/denk_mal/bash_scripts
#
# this is a script to excute scripts starting with a number of a folder
# in a given order.
# The order is given by the filename e.g. 01_first.sh, 02_second.sh and
# could handle any kind of excutables (shell, python, binary, ...).
# Every script can control if other scripts should be executed afterward.
# The exit code of those scripts controls the execute behavior of this
# main script.
# exit = 0 => no execution of further scripts necessary;
#             script will stop gracefully.
# exit = 1 => no error on execute but running of other scripts necessary
#             will send out an email after last script.
# exit > 1 => an error occured;
#             script will stop and send out an email.
# for environment setup this script sourced a file called 'local.env' from
# the folder containing this script.
# the target email address is taken from the var MAIL_RECEIVER or MAILTO if
# MAIL_RECEIVER is not set. This variable could be set in e.g. the crontab
# or the 'local.env' file in the folder of this script.

DATE_FORMATSTR="${DATE_FORMATSTR-%F %T: }"

# some common default settings
SCRIPT_FULLPATH=${0:A}
SCRIPT_BASE_DIR=${0:A:h}
#SCRIPT_NAME=${0:t}

# overwrite above presets in a local settingsfile
# shellcheck source=/dev/null
test -s "${SCRIPT_BASE_DIR}/local.env" && source "${SCRIPT_BASE_DIR}/local.env"

MAIL_RECEIVER="${MAIL_RECEIVER:-$MAILTO}"

function log_and_exit() {
  local exit_status=${1}
  log_msg=${2}

  if [ "${exit_status}" -ne 1 ]; then
    printf "%s\n" "${(P)${log_msg}[@]}"
    #|/usr/bin/mail -s "[certificate] state:${exit_status}" "${MAIL_RECEIVER}"
  fi

  exit "${exit_status}"
}

CMD_DIR="${1}"

messages=()
if [ ! -d "${CMD_DIR}" ]; then
  messages+=( "dir '${CMD_DIR}' not existing!" )
  log_and_exit 2 "messages"
fi

SCRIPT_BASE_DIR="${SCRIPT_BASE_DIR}/${CMD_DIR}"

retval=0
act_script=""
for act_script in ${CMD_DIR}/[0-9]*; do
  script_msg=$("${act_script}" -q); retval="${?}"
  messages+=( "${script_msg}" )
  if [ $retval -ne 0 ]; then
    break;
  fi
done

case ${retval} in
  0) message=$(date +"${DATE_FORMATSTR}state ok: script ok.") ;;
  1) message=$(date +"${DATE_FORMATSTR}state ok: '${act_script}': no further action necessary.") ;;
  *) message=$(date +"${DATE_FORMATSTR}state fail => ${retval}: '${act_script}' error occured!") ;;
esac
messages+=("${message}")

log_and_exit ${retval} "messages"
