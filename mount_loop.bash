#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#
# partial code taken from https://github.com/Drewsif/PiShrink
#

DATE_FORMATSTR="${DATE_FORMATSTR-%F %T: }"

info() {
  date +"${DATE_FORMATSTR}${1}"
}

error() {
  info "ERROR: ${1}"
  exit 1
}

usage() {
  info "${1}Usage: ${0} (mount|unmount) IMAGEFILE [[MOUNTPOINT] PART_NUM]"
  exit 0
}

get_loopdevice() {
  img="${1}"
  losetup -j "${img}"|cut -f1 -d':'
}

get_mountpoint() {
  loopdevice=$(get_loopdevice "${1}")
  grep "^${loopdevice} " /proc/mounts | cut -d ' ' -f 2
}

mount_img() {
  img="${1}"
  part_num="${2}"
  mountdir="${3}"
  info "try to loop mount '${img}:${part_num}' on '${mountdir}'..."
  mountpoint=$(get_mountpoint "${img}")
  if [ "${mountpoint}" != "" ]; then
    error "'${img}' is already mounted"
  fi
  parted_output="$(parted -ms "${img}" unit B print)"
  rc=$?
  if [ "${rc}" -ne 0 ]; then
    info ${LINENO} "parted failed with rc ${rc}"
    error "Possibly invalid image. Run 'parted ${img} unit B print' manually to investigate"
  fi
  partstart="$(echo "${parted_output}" | grep "^${part_num}" | cut -d ':' -f 2 | tr -d 'B')"
  loopback="$(losetup -f --show -o "${partstart}" "${img}")"
  #tune2fs_output="$(tune2fs -l "${loopback}")"
  #rc=$?
  #if [ "${rc}" -ne 0 ]; then
  #  info "${tune2fs_output}"
  #  error ${LINENO} "tune2fs failed. Unable to mount this type of image"
  #fi

  info "check mount dir '${mountdir}'"
  if [ ! -d "${mountdir}" ]; then
    mkdir "${mountdir}"
  fi
  if [ ! -d "${mountdir}" ]; then
    error "Can't create dir for mount point '${mountdir}'!"
  fi
  mount "${loopback}" "${mountdir}"
  info "loop mount ${img} to ${mountdir}"

}

unmount_img() {
  img="${1}"
  loopback=$(get_loopdevice "${img}")
  mountdir=$(get_mountpoint "${img}")
  info "try to unmount '${mountdir}'..."
  umount "${mountdir}"
  info "try to remove loopback device '${loopback}'..."
  losetup -d "${loopback}"
}

prepare_action() {
  if [ "${#}" -lt 2 ]; then
    usage "Missing image file name; "
  fi
  img=$(realpath -e "${2}")
  if [ "${#}" -ge 3 ]; then
    mountdir=$(realpath -e "${3}")
  else
    mountdir=$(mktemp -d)
  fi
  if [ "${mountdir}" = "" ]; then
    error "mountdir is not accessable '${3}'"
  fi
  case ${1} in
    "mount")
      part_num="${4:-2}"
      mount_img "${img}" "${part_num}" "${mountdir}"
      ;;
    "unmount")
      unmount_img "${img}"
      ;;
    *)
      ;;
  esac
}

info "start"
case ${1} in
  "mount"|"unmount")
    prepare_action "${@}"
    info "ready"
    ;;
  -h|--help)
    usage ""
    ;;
  *)
    usage "Wrong command; "
esac
