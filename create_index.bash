#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#
# Filename: imggallery.sh
# Version: 0.2
# Date: Thu Feb  12 21:21:28 PST 2009
# Author: Ian MacGregor (aka ardchoille)
#         Frank Nießen
# License: GPL
# This script creates a web gallery from specified images
# Version: 0.2:
#     - add page title as parameter
#     - convert all images files to lower case

BASEFOLDER="." #$(pwd)
TARGET_PAGE="index.html"
THUMB_FOLDER="thumbnails"
DATEFORMAT="%d. %B %Y"
TIMEFORMAT="%H:%M:%S"

FOLDERS=()
declare -A FOLDERS2
FILE_EXTENSIONS="(jpg|jpeg|png|gif|tif|tiff)"

output_timestamp=$(date +%s)
output_log() {
  msg="${1}"
  tmp_timestamp=$(date +%s)
  diff=$((tmp_timestamp - output_timestamp))

  if [ ${diff} -ge 10 ] || [[ -n "${2}" ]]; then
    date +"${TIMEFORMAT}: ${msg}"
  fi
  output_timestamp=tmp_timestamp
}


# ${1} : targetfile (index.html)
# ${2} : foldername
create_index_html() {
  old_dir=$(pwd)
  cd "${2}" || return
  # dir_name="${old_dir##*/}"
  output_log "create index.html in '${2}'"
  echo "<html>" > "${1}"
  {
  echo "<head>"
  echo "<title>Bilder des Ordners '${2}'</title>"
  echo "<meta charset=\"UTF-8\">"
  echo "</head>"
  echo "<body>" 
  } >> "${1}"

  for dir in *; do
    if [ -d "${dir}" ] && [ "${dir}" != "${THUMB_FOLDER}" ]; then
      for dir2 in "${FOLDERS2[@]}"; do
        full_dir="${2}/${dir}"
        if [ "${full_dir}" = "${dir2}" ] ; then
          prefix="${dir2#"${2}"}"
          prefix="${prefix:1}"
          if [[ "${prefix}" != *"/"* ]]; then
            echo "<a href=\"${prefix}/index.html\">${prefix}</a><br>" >> "${1}"
          fi
        fi
      done
    fi
  done

  {
  echo "<hr>"
  echo "<h1>Bilder des Ordners '${2}'</h1>"
  } >> "${1}"

  for img in *.{jpg,jpeg,png,gif,tif,tiff} ; do
    if [[ "${img}" != \** ]]; then
      output_log "convert '${img}'"
      convert -scale 120 "${img}" "${THUMB_FOLDER}/thumb-${img}"
      # mogrify -scale 640 "${img}" "${THUMB_FOLDER}/thumb-${img}"
      echo "<a href=\"${img}\" target=\"_blank\"><img src=\"${THUMB_FOLDER}/thumb-${img}\"></a>" >> "${1}"
    fi
  done

  {
  echo "<h3>Bitte die Bilder anklicken für große Darstellung.</h3>"
  echo "---<br>"
  date +"page automatically created on ${DATEFORMAT} ${TIMEFORMAT}"
  echo "</body>"
  echo "</html>"
  } >> "${1}"
  cd "${old_dir}" || return
}

# ${1} : foldername
prepare_image_folder() {
  shopt -s nocaseglob
  count=$(find . -maxdepth 1 -regextype posix-extended -iregex ".*\.${FILE_EXTENSIONS}"|wc -l)
  if [ "${count}" != 0 ]; then
    FOLDERS+=("${1}")
    old_dir=$(pwd)
    cd "${1}" || return
    mkdir -p "${THUMB_FOLDER}" &> /dev/null
    cd "${old_dir}" || return
  fi
  shopt -u nocaseglob
}

scan_subdir() {
  mapfile -t array < <(ls -d1 "${1}"/* 2>/dev/null)
  for dir in "${array[@]}"; do
    if [ -d "${dir}" ] && [ "${dir##*/}" != "${THUMB_FOLDER}" ]; then
      output_log "prepare ${dir}..."
      prepare_image_folder "${dir}"
      scan_subdir "${dir}"
    fi
  done
}

output_log "start scan of '${BASEFOLDER}'..." "true"
# make all pic filenames lowercase
find . -regextype posix-extended -iregex ".*\.${FILE_EXTENSIONS}" \
    -exec bash -c 'path=${1}; file=${path##*/}; mv "${path}" "${path%/*}/${file,,}"' _ {} \;
# for file_ext in "${FILE_EXTENSIONS[@]}"; do
  # find . -type f \( -name "*[[:upper:]]*" -a -iname "*.${file_ext}" \) \
  #   -exec bash -c 'path=${1}; file=${path##*/}; mv "${path}" "${path%/*}/${file,,}"' _ {} \;
# done
# scan dir structure
output_log "start scandir..." "true"
scan_subdir "${BASEFOLDER}"
output_log "prepare image folders..." "true"
prepare_image_folder "${BASEFOLDER}"

output_log "collect all subfolders" "true"
# collect all folders that need a html page
FOLDERS2["${BASEFOLDER}"]="${BASEFOLDER}"
for dir in "${FOLDERS[@]}"; do
  FOLDERS2["${dir}"]="${dir}"

  # echo "check ${dir}:"
  parentdir="${dir%/*}"
  while [ "${parentdir}" != "${BASEFOLDER}" ]; do
    FOLDERS2["${parentdir}"]="${parentdir}"
    parentdir="${parentdir%/*}"
  done
done

output_log "start creating of thumbnails and html pages" "true"
for dir in "${FOLDERS2[@]}"; do
  output_log "create '${TARGET_PAGE}' in '${dir}'" "true"
  create_index_html "${TARGET_PAGE}" "${dir}"
done
output_log "ready." "true"
