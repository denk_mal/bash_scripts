#!/usr/bin/env bash

start_app_image()
{
  # execute AppImage
  AppImage_Basepath=""
  if [ -f "${HOME}/.config/appimagelauncher.cfg" ]; then
    AppImage_Basepath=$(grep "^destination=" "${HOME}/.config/appimagelauncher.cfg"|cut -d"=" -f2)
  fi
  appimage_cmd=$(find "${AppImage_Basepath}" -iname "${1}*"|sort -n|tail -n1)
  "${appimage_cmd}" &
}

start_app_image "${1}"
