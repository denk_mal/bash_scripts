#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#

apt-get update &> /dev/null
apt-get -s upgrade| awk -F'[][() ]+' '/^Inst/{printf "Prog: %s\tcur: %s\tavail: %s\n", $2,$3,$4}'
