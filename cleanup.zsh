#!/usr/bin/env zsh
#
# https://gitlab.com/denk_mal/bash_scripts
#

FOUND_SOMETHING="--NO--"

log_output() {
  lines=("${(f)1}")

  if [ "${2}" != "" ]; then
    local i=$((${#1}+20))
    local ul_string=${2}
    while [ ${#ul_string} -lt ${i} ]; do
      ul_string=${ul_string/${2}/${2}${2}}
    done
  fi

  for line in "${lines[@]}"; do
    date +"%Y/%m/%d %H:%M:%S ${line}"
  done
  if [ "${ul_string}" != "" ]; then echo "${ul_string}"; fi
}

cleanup_logs() {
  local exts=("gz" "bz" "xz" "old" "[0123456789]")

  if [ "${1}" = "delete" ]; then
    log_output "cleanup logs" "="
    local param="-delete"
  else
    log_output "logs to cleanup" "="
  fi

  for ext in "${exts[@]}"; do
    local out=$(find /var/log -iname "*\.${ext}" ${param})
    if [ "${out}" != "" ]; then
      echo ">>>>> '${out}'"
      FOUND_SOMETHING="--YES--"
    fi
  done

  local out=$(find /var/log -type f -mtime +90 ${param})
  if [ "${out}" != "" ]; then
    echo "${out}"
    FOUND_SOMETHING="--YES--"
  fi

  echo
}

cleanup_dpkg() {
  if [ "${1}" = "delete" ]; then
    log_output "cleanup debian packages (remove rc)" "="
    dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs -r dpkg --purge
  else
    log_output "packages to cleanup" "="
    local out=$(dpkg --list | grep "^rc" | cut -d " " -f 3)
    if [ "${out}" != "" ]; then
      echo "${out}"
      FOUND_SOMETHING="--YES--"
    fi
  fi
  echo
}

cleanup_container() {
  local containerapp="${1}"
  if ! [ -x "$(command -v "${containerapp}")" ]; then
    log_output "no ${containerapp} installed."
    return
  fi
  if [ "${2}" = "delete" ]; then
    log_output "cleanup ${containerapp} container" "="
    ${containerapp} system prune -af
    ${containerapp} volume ls -q -f 'dangling=true' | xargs -r "${containerapp}" volume rm
  else
    log_output "${containerapp}-container to cleanup" "="
    local out=$(${containerapp} ps -aq -f 'status=exited' 2>/dev/null)
    if [ "${out}" != "" ]; then
      echo "${out}"
      FOUND_SOMETHING="--YES--"
    fi
    out=$(${containerapp} images -aq -f 'dangling=true' 2>/dev/null)
    if [ "${out}" != "" ]; then
      echo "${out}"
      FOUND_SOMETHING="--YES--"
    fi
    out=$(${containerapp} volume ls -q -f 'dangling=true' 2>/dev/null)
    if [ "${out}" != "" ]; then
      echo "${out}"
      FOUND_SOMETHING="--YES--"
    fi
  fi
  echo
}

cleanup_docker() {
  cleanup_container docker "${1}"
}

cleanup_podman() {
  cleanup_container podman "${1}"
}

cleanup_logs "${1}"
cleanup_dpkg "${1}"
cleanup_docker "${1}"
cleanup_podman "${1}"

if [ "${1}" != "delete" ] && [ "${FOUND_SOMETHING}" = "--YES--" ]; then
  echo
  echo "=================================================="
  echo "restart with parameter 'delete' for a real cleanup"
fi
