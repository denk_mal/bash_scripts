#compdef dc-control

# get commands from dc-control
local compl_str=$(dc-control --compl_zsh)

_arguments "1:base dir:_dir_list"\
  "2:command:compadd -- ${compl_str}"
