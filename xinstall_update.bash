#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#

DATE_FORMATSTR="${DATE_FORMATSTR-%F %T: }"

CHECK_ONLY="--NO--"
VERBOSE="--YES--"
REBOOT_REQUIRED="--NO--"

# this will NOT resolve symlinks to this script and made it therefore
# possible to put the xinstall_update_post.sh beside the symlink
FULLPATH_SCRIPT=$(realpath -se "${0}")
BASE_DIR=${FULLPATH_SCRIPT%/*}
SCRIPT_NAME=${FULLPATH_SCRIPT##*/}

check_for_restart() {
  while read -r line; do
    needle=${line/DEL*/}
    if [[ "${line##"$needle"}" = *"lib"* ]]; then
      REBOOT_REQUIRED="--YES--"
      if [ "${VERBOSE}" = "--YES--" ]; then
        date +"${DATE_FORMATSTR}${line}"
      else
        break
      fi
    fi
  done < <(lsof -w|grep "DEL")

  if [ "${REBOOT_REQUIRED}" = "--NO--" ]; then
    date +"${DATE_FORMATSTR}No reboot is required."
  else
    date +"${DATE_FORMATSTR}A reboot is required!"
  fi
}

while getopts "vos" opt; do
  case ${opt} in
    o)
      CHECK_ONLY="--YES--"
      ;;
    v)
      VERBOSE="--YES--"
      ;;
    s)
      VERBOSE="--NO--"
      ;;
    *)
      ;;
  esac
done

if [ "${CHECK_ONLY}" = "--NO--" ]; then
  # perform update of flatpak apps
  if command -v flatpak &> /dev/null; then
    flatpak update --noninteractive
  fi

  apt-get update
  cnt=$(apt-get -s dist-upgrade | awk '/^Inst/ { print $2 }'|wc -l)
  if [ "${cnt}" -eq 0 ]; then
    date +"${DATE_FORMATSTR}no upgrades available. will stop now!"
    check_for_restart
    exit 0
  fi
  apt-get -y dist-upgrade
fi

date +"${DATE_FORMATSTR}###############################################"

check_for_restart

POST_COMMANDS="${BASE_DIR}/${SCRIPT_NAME%.*}_post.sh"
if [ -f "${POST_COMMANDS}" ]; then
  # shellcheck source=./xinstall_update_post.sh
  . "${POST_COMMANDS}"
fi
