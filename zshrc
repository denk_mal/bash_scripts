# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=2000
setopt autocd extendedglob nomatch rmstarsilent
unsetopt beep notify
bindkey -e
# End of lines configured by zsh-newuser-install

# must be set before call of compinit; otherwise completion would not work
fpath=(/opt/zsh/completions/src $fpath)

# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit && compinit
setopt autolist

# End of lines added by compinstall

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

[[ ! -f /opt/zsh/powerlevel10k/powerlevel10k.zsh-theme ]] || source /opt/zsh/powerlevel10k/powerlevel10k.zsh-theme

[[ ! -f /opt/zsh/autosuggestions/zsh-autosuggestions.zsh ]] || source /opt/zsh/autosuggestions/zsh-autosuggestions.zsh

# some history settings
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space

# some bashisms
bindkey "^[[1;5C" vi-forward-word
bindkey "^[[1;5D" vi-backward-word
## default WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>'
## WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

# standard settings working on bash and zsh
[[ ! -f ~/.aliases ]] || source ~/.aliases
[[ ! -f ~/.zsh_local ]] || source ~/.zsh_local
[[ ! -f /opt/bash_scripts/optimize_path ]] || source /opt/bash_scripts/optimize_path

# should be the last command
command -v direnv &>/dev/null && eval "$(direnv hook zsh)"
