#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#

declare -A ARGS_LONG
ARGS_LONG=(
  [cl]="clean"
  [co]="cold-restart"
  [de]="destroy"
  [do]="down"
  [r]="restart"
  [star]="start"
  [stat]="status"
  [sto]="stop"
  [u]="up"
  [w]="warm-restart"
)

declare -A ARGS_DESC
ARGS_DESC=(
  [cl]="'down' compose project and force pruning volumes"
  [co]="'destroy' and 'start' compose project"
  [de]="stops compose project and remove images and orphan container"
  [do]="stops compose project and remove orphan container. executing pre_down.sh and post_down.sh scripts"
  [r]="'stop' and 'start' compose project"
  [star]="build and starts up compose project incl. executing pre_start.sh and post_start.sh scripts"
  [stat]="prints out status of compose project"
  [sto]="stops compose project incl. executing pre_stop.sh and post_stop.sh scripts"
  [u]="same as 'start'"
  [w]="'down' and 'start' compose project"
)

FULLPATH_SCRIPT=$(realpath -e "${0}")
# BASE_DIR=${FULLPATH_SCRIPT%/*}
SCRIPT_NAME="${FULLPATH_SCRIPT##*/}"

function join_arry { local IFS="${1}"; shift; echo "${*}"; }

function description() {
  echo "Usage:  ${SCRIPT_NAME} <project-folder path> COMMAND"
  echo "    <project-folder path>    path to folder containing the docker-compose yaml file"
  echo
  echo "    COMMAND"
  for arg in $(<<< "${!ARGS_LONG[@]}" tr ' ' '\n' | sort); do
    printf "    %-15s --> %s\n" "${ARGS_LONG[${arg}]}" "${ARGS_DESC[${arg}]}"
  done
}

### associative commands section #############################################
### --> ${ARGS_LONG[${arg}]}_docker()
function clean_docker() {
  docker compose down --remove-orphans
  docker volume prune --force
}

function cold-restart_docker() {
  destroy_docker
  start_docker
}

function destroy_docker() {
  docker compose down --rmi='all' --remove-orphans
}

function down_docker() {
  [ -f ./pre_down.sh ] && ./pre_down.sh
  docker compose down --remove-orphans
  [ -f ./post_down.sh ] && ./post_down.sh
}

function restart_docker() {
  stop_docker
  start_docker
}

function start_docker() {
  [ -f ./pre_start.sh ] && ./pre_start.sh
  docker compose build --pull
  docker compose up -d --quiet-pull
  [ -f ./post_start.sh ] && ./post_start.sh
}

function status_docker() {
  docker compose ps
}

function stop_docker() {
  [ -f ./pre_stop.sh ] && ./pre_stop.sh
  docker compose stop
  [ -f ./post_stop.sh ] && ./post_stop.sh
}

function up_docker() {
  start_docker
}

function warm-restart_docker() {
  down_docker
  start_docker
}
##############################################################################

if [ "${#}" -ne 2 ]; then
  case "${1##*-}" in
    "h"|"help")
      description
      ;;
    "compl_bash")   # 'invisible' call option for bash copletion
      # shellcheck disable=SC2046
      params=$(join_arry '|' $(<<< "${ARGS_LONG[@]}" tr ' ' '\n' | sort))
      echo "Call: ${0##*/} <containername> (${params})"
      ;;
    "compl_zsh")    # 'invisible' call option for zsh copletion
      echo "${ARGS_LONG[@]}"
      ;;
    *)
      echo "wrong number of arguments"
      description
      exit 255
  esac
  exit 0
fi

# correct parameter from autocomplete
APP_PATH="${1}"
if [[ "${APP_PATH}" == */ ]]; then
  APP_PATH=${APP_PATH::-1}
fi
if [[ "${APP_PATH}" == *app ]]; then
  APP_PATH=${APP_PATH::-3}
fi

# check if basefolder exists
if [ ! -d "${APP_PATH}" ]; then
  APP_PATH="${APP_PATH}app"
  if [ ! -d "${APP_PATH}" ]; then
    echo "basefolder not existing!"
    exit 255
  fi
fi

cd "${APP_PATH}" || exit 255

# check if docker compose yaml file exists
YAML_FILE=$(find . -maxdepth 1 -regex ".*\(docker-\)?compose.ya?ml" -printf "%f\n")
if [ "${YAML_FILE}" = "" ]; then
  echo "missing/empty docker-compose file in directory '${APP_PATH}'"
  exit 255
fi

if ! docker compose config --quiet; then
  echo "exit now; docker compose file '${YAML_FILE}' contains error(s)"
  exit 255
fi

for key in "${!ARGS_LONG[@]}"; do
  if [[ "${2}" == "${key}"* ]]; then
    func_name="${ARGS_LONG[${key}]}_docker"
    break
  fi
done

if [ "${func_name}" != "" ]; then
  echo "call ${func_name}" >&2
  ${func_name}
else
  description
fi

exit 0
