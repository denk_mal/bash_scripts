# System Helper Scripts
A collection of small bash scripts for (not only) debian based systems.

* args_helper.bash: bash args parser script with a more comfortable using
* args_helper_tester.bash: example implementation for args_helper.bash
* autostart.bash: a simple script that starts up a bunch of tasks/apps delayed
* bashrc/aliases: templates for .bashrc/.aliases
* check_for_updates.bash: check and list possible system updates (debian/apt-get)
* cleanup.bash: cleanup log files, debian packages and docker container
* create_index.bash: script for creating html files in a picture basefolder
* dc-control.bash: start/stop/restart docker-compose container with or without cleanup and renewing container
* dc-control.sh.bash: bash completion file
* dc-control.sh.zsh: zsh completion file
* gitup.bash: updating a git repository with automatic stashing of local changes
* mount_loop.bash: little script for mounting and unmounting an SBC (Raspberry Pi) sd-card .img file
* msmtp.bash: a msmtp helper script to work with msmtp and /etc/aliases; rename original msmtp to msmtp.bin
* optimize_path: a source file to remove duplicate entries in the path
* start_app_image.bash: starts up an App Image installed by appimagelauncher
* script-runner.bash: conditional execution of scripts from a folder given as a parameter
* xinstall_update.bash: check and optionally install update; check for a necessary restart of services/computer
* xinstall_update_post.sh: script will be sourced at the end of xinstall_update.bash for local past update tasks
* zshrc: template for .zshrc
* helper folder: example scripts for script-runner.bash
