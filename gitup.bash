#!/usr/bin/env bash

# script for updating a local git repository

if ! git status &>/dev/null ; then
  date +"%F %T: This is not a valid git repository!"
  exit 255
fi

current_branch=$(git branch --show-current 2>/dev/null)
if [ "${current_branch}" = "" ]; then
  date +"%F %T: no valid actual git branch found!"
  exit 254
fi

restore_from_stash=$(git diff|wc -l)

if [ "${restore_from_stash}" != 0 ]; then
  git stash push --message="git up <automatic stash>"
fi

for branchname in $(git branch|cut -c2-); do
  if [ $"${branchname}" = "${current_branch}" ]; then
    continue
  fi
  git checkout "${branchname}"
  git fetch --prune
  git pull
done

git checkout "${current_branch}"
if [ -s ".gitmodules" ]; then
  date +"%F %T: ------------------ update submodules -------------------"
  git submodule update --init --recursive
  date +"%F %T: ---------------- update submodules end -----------------"
fi

git pull
if [ "${restore_from_stash}" != 0 ]; then
  git stash pop
fi

date +"%F %T: -------------------- current status --------------------"

git status
