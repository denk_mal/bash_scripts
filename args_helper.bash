# this script parse the input parameters and checks for necessary flags and
# returns them in variables
#
# include this script at the beginning of your script with
# source args_helper.bash
#
# input:
# ======
# ARGS_HELPER_IN_SHORT_FLAGS: Array of short flags
# ARGS_HELPER_IN_LONG_FLAGS:  Array of long flags
# ARGS_HELPER_IN_HAS_VALUE: Array of boolean indicating if flags needs a parameter
# ARGS_HELPER_IN_IS_MANDATORY: Array of boolean indicating if flags is mandatory (only usefull if flag has parameter)
# ARGS_HELPER_IN_DESCRIPTION: Array of descriptions
#
# output:
# =======
# ARGS_HELPER_OUT_FLAG_XXX:    Variable for flags (XXX is the upper case long flag)
# ARGS_HELPER_OUT_VALUES:      Array with flag parameter
# ARGS_HELPER_OUT_UNHANDLED_ARGS:         all not handled parameters
#
# example:
# ========
#
# ARGS_HELPER_IN_SHORT_FLAGS=( f t r "" )
# ARGS_HELPER_IN_LONG_FLAGS=( find test read timezone )
# ARGS_HELPER_IN_HAS_VALUE=( false false true true )
# ARGS_HELPER_IN_IS_MANDATORY=( false false true false )
# ARGS_HELPER_IN_DESCRIPTION=(
#    "find flag"
#    "test flag"
#    "read from file <name>"
#    "timezone" )
#
# call with
# $> ARGS_PARSE_parse_args -t --read name param2 --timezone GMT
#
# results in
#
# ARGS_HELPER_OUT_FLAG_FIND="--NO--"
# ARGS_HELPER_OUT_FLAG_TEST="--YES--"
# ARGS_HELPER_OUT_FLAG_READ="--YES--"
# ARGS_HELPER_OUT_VALUES["read"]="name"
# ARGS_HELPER_OUT_FLAG_TIMEZONE="--YES--"
# ARGS_HELPER_OUT_VALUES["timezone"]="GMT"
# ARGS_HELPER_OUT_UNHANDLED_ARGS="param2"
#
# this script needs bash 4.0 or higher!
#
# Version 1.4 (2018/04)
# add ${ARGS_HELPER_DATETIME_FMT} for customizing datetime output of log printouts
#
# Version 1.3 (2011/05)
# add flagfield for mandatory parameters
#
# Version 1.2 (2011/01)
# full POSIX conform (can now have long option w/o short option
#
# Version 1.2 (2011/01)
# add base dir handling (${ARGS_HELPER_OUT_BASE_DIR}, ${ARGS_HELPER_OUT_BASE_SCRIPT})
#
# Version 1.1 (2010/12)
# add verbose tracking
#
# Version 1.0 (2010/05)
# Initial release
#
# https://gitlab.com/denk_mal/bash_scripts
#

declare -A ARGS_HELPER_OUT_VALUES
ARGS_HELPER_OUT_VERBOSE=1
readonly ARGS_HELPER_FLAG_TRUE="--YES--"
readonly ARGS_HELPER_FLAG_FALSE="--NO--"

result=$(realpath -e "${0}") || exit 1
readonly ARGS_HELPER_OUT_BASE_SCRIPT="${result}"
# shellcheck disable=SC2034  # this var could be used by the script that sourced this file
readonly ARGS_HELPER_OUT_BASE_DIR=${ARGS_HELPER_OUT_BASE_SCRIPT%/*}
# shellcheck disable=SC2034  # this var could be used by the script that sourced this file
readonly ARGS_HELPER_OUT_BASE_SCRIPT_NAME=${ARGS_HELPER_OUT_BASE_SCRIPT##*/}

if [ "${ARGS_HELPER_DATETIME_FMT}" = "" ]; then
  ARGS_HELPER_DATETIME_FMT="%Y/%m/%d %H:%M:%S"
fi

# filter functions for message output depending on verbose level
echo_fatal() {
  if [ ${ARGS_HELPER_OUT_VERBOSE} -ge 1 ]; then
    date +"${ARGS_HELPER_DATETIME_FMT} ${1}"
  fi
}

echo_error() {
  if [ ${ARGS_HELPER_OUT_VERBOSE} -ge 2 ]; then
    date +"${ARGS_HELPER_DATETIME_FMT} ${1}"
  fi
}

echo_warning() {
  if [ ${ARGS_HELPER_OUT_VERBOSE} -ge 3 ]; then
    date +"${ARGS_HELPER_DATETIME_FMT} ${1}"
  fi
}

echo_info() {
  if [ ${ARGS_HELPER_OUT_VERBOSE} -ge 4 ]; then
    date +"${ARGS_HELPER_DATETIME_FMT} ${1}"
  fi
}

echo_debug() {
  if [ ${ARGS_HELPER_OUT_VERBOSE} -ge 5 ]; then
    date +"${ARGS_HELPER_DATETIME_FMT} ${1}"
  fi
}

# dummy procedure
# over write this in the calling script
description() {
  echo "<no description>"
  echo
}

# parse the ARGS_HELPER_IN_SHORT_FLAGS array for all possible args and
# print them out; is using callback description
ARGS_HELPER_usage() {
  echo "usage: ${0} options"
  echo

  description

  local v_is_used=false
  local linefiller="                "
  local desc_dist=14
  echo "OPTIONS: (* are mandatory)"
  echo "   -h,--help${linefiller:0:${desc_dist}-4}this help text"
  for ((ii=0; ii<${#ARGS_HELPER_IN_LONG_FLAGS[*]}; ii++)) do
    local p_short="-${ARGS_HELPER_IN_SHORT_FLAGS[${ii}]}"
    local p_long="--${ARGS_HELPER_IN_LONG_FLAGS[${ii}]}"
    local p_desc=${ARGS_HELPER_IN_DESCRIPTION[${ii}]}
    local p_mandatory=""
    if [ "${ARGS_HELPER_IN_IS_MANDATORY[${ii}]}" = true ]; then
      p_mandatory="*";
    fi
    if [ "${p_short}" = "-" ]; then
      echo "      ${p_long} ${linefiller:0:${desc_dist}-${#p_long}} ${p_mandatory} ${p_desc}";
    else
      echo "   ${p_short},${p_long} ${linefiller:0:${desc_dist}-${#p_long}} ${p_mandatory} ${p_desc}";
    fi
    if [ "${p_short}" = "-v" ] || [ "${p_long}" = "--verbose" ]; then
        v_is_used=true
    fi
  done
  if [ ${v_is_used} = false ]; then
    echo "   -v,--verbose${linefiller:0:${desc_dist}-7}increase verbose level"
  fi
}

# initialize the all ARGS_HELPER_OUT_FLAG_XXX variables to ARGS_HELPER_FLAG_FALSE
ARGS_HELPER_init() {
  if [ "${BASH_VERSINFO[0]}" -lt 4 ]; then
    echo "The bash must be version 4.x or higher!"
    exit 255
  fi
  declare -u var_name
  for var_name in "${ARGS_HELPER_IN_LONG_FLAGS[@]}"; do
    eval "ARGS_HELPER_OUT_FLAG_${var_name}"=${ARGS_HELPER_FLAG_FALSE}
  done
}

# extract the next flag and parameter if any
ARGS_HELPER_check_flag_and_parameter() {
  declare -u var_name
  local flag_found=false
  local flag_uses_value=false
  local switch=${1}
  shift

  for ((ii=0; ii<${#ARGS_HELPER_IN_LONG_FLAGS[*]}; ii++)) do
    local cmp_value
    if [ "${switch}" = "SHORT" ]; then
      cmp_value="-${ARGS_HELPER_IN_SHORT_FLAGS[${ii}]}"
    else
      cmp_value="--${ARGS_HELPER_IN_LONG_FLAGS[${ii}]}"
    fi
    if [ "${param_part}" = "${cmp_value}" ]; then
      var_name="ARGS_HELPER_OUT_FLAG_${ARGS_HELPER_IN_LONG_FLAGS[${ii}]}"
      eval "${var_name}"=${ARGS_HELPER_FLAG_TRUE}
      flag_found=true
      if [ "${ARGS_HELPER_IN_HAS_VALUE[${ii}]}" = true ]; then
        shift
        if [ "${1:0:1}" = "-" ] || [ -z "${1}" ]; then
          echo "missing parameter for ${param_part}"
          ARGS_HELPER_usage
          exit 2
        fi
        ARGS_HELPER_OUT_VALUES["${ARGS_HELPER_IN_LONG_FLAGS[${ii}]}"]=${1}
        flag_uses_value=true
      fi
    fi
  done

  if [ ${flag_found} = false ]; then
    case "${param_part}" in
      "-v")
        (( ARGS_HELPER_OUT_VERBOSE=ARGS_HELPER_OUT_VERBOSE+1 ))
        ;;
      "--verbose")
        (( ARGS_HELPER_OUT_VERBOSE=ARGS_HELPER_OUT_VERBOSE+1 ))
        ;;
      --*)
        echo "unknown long flag ${param_part}"
        ;;
      *)
        echo "unknown short flag ${param_part}"
        ;;
    esac
  fi

  if [ ${flag_uses_value} = true ]; then
    return 1
  fi
  return 0
}

# main routine
ARGS_HELPER_parse_args() {
  ARGS_HELPER_init

  local combined_part=""
  local param_part=${1}
  while [ -n "${param_part}" ]; do
    if [ -n "${combined_part}" ]; then
      param_part=${combined_part:0:2}
      if [ ${#combined_part} -gt 2 ]; then
        combined_part="-"${combined_part:2}
      else
        combined_part=""
      fi
    fi

    case "${param_part}" in
      "-h"|"--help"|"-?")
        ARGS_HELPER_usage
        exit 1
        ;;
      # filter unknown and combined flags
      -*)
        # check if long arg (--)
        if [ "${param_part:1:1}" = "-" ]; then
          if ! ARGS_HELPER_check_flag_and_parameter "LONG" "${@}"; then
            shift
          fi
        else
          # check if combined arg (e.g. -ft as shortform of -f -t)
          if [ ${#param_part} -gt 2 ]; then
            combined_part=${param_part}
          else
            if ! ARGS_HELPER_check_flag_and_parameter "SHORT" "${@}"; then
              shift
            fi
          fi
        fi
        ;;
      # filter any other parameter that is not a flag
      *)
        # enclose in " if param_part contains spaces

        if [[ ${param_part} =~ " " ]]; then
          param_part="\"${param_part}\""
        fi
        if [ -z "${ARGS_HELPER_OUT_UNHANDLED_ARGS}" ]; then
          ARGS_HELPER_OUT_UNHANDLED_ARGS=${param_part}
        else
          ARGS_HELPER_OUT_UNHANDLED_ARGS="${ARGS_HELPER_OUT_UNHANDLED_ARGS} ${param_part}"
        fi
        ;;
    esac

    if [ -z "${combined_part}" ]; then
      shift
      param_part=${1}
    fi
  done

  for ((ii=0; ii<${#ARGS_HELPER_IN_LONG_FLAGS[*]}; ii++)) do
    if [ "${ARGS_HELPER_IN_HAS_VALUE[${ii}]}" = true ]; then
      long_flag=${ARGS_HELPER_IN_LONG_FLAGS[${ii}]}
      if [ "${ARGS_HELPER_IN_IS_MANDATORY[${ii}]}" = true ]; then
        if [ "${ARGS_HELPER_OUT_VALUES["${long_flag}"]}" = "" ]; then
          echo "mandatory parameter \"${long_flag}\" is missing!"
          echo
          ARGS_HELPER_usage
          exit 3
        fi
      fi
    fi
  done
}
