#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#
# call examples:
#
# $ ./args_helper_tester.bash
# $ ./args_helper_tester.bash  -r
# $ ./args_helper_tester.bash  -r testfile.txt
# $ ./args_helper_tester.bash  -vf -r testfile.txt renew -wg yoyoyo
# $ ./args_helper_tester.bash  -vvvf -r testfile.txt renew -wg yoyoyo
# $ ./args_helper_tester.bash  -vvvfwgr testfile.txt renew yoyoyo
#
# set -x
#
# args_helper section begin
source args_helper.bash

# shellcheck disable=SC2034
ARGS_HELPER_IN_SHORT_FLAGS=( f w g t r "" )
# shellcheck disable=SC2034
ARGS_HELPER_IN_LONG_FLAGS=( force win gtk test read tester )
# shellcheck disable=SC2034
ARGS_HELPER_IN_HAS_VALUE=( false false false false true true )
# shellcheck disable=SC2034
ARGS_HELPER_IN_IS_MANDATORY=( false false false false true false )
# shellcheck disable=SC2034
ARGS_HELPER_IN_DESCRIPTION=(
  "force a build (even if svn is actual)"
  "build for mingw win"
  "build for gtk"
  "test configure process"
   "read from file <name>"
  "testuser")

description()
{
  echo "This script <${0}> build the wxWidgets environment from svn for version ${WX_VERSION}"
  echo
}

ARGS_HELPER_parse_args "${@}"
# args_helper section end;

declare -u var_name
for var_name in "${ARGS_HELPER_IN_LONG_FLAGS[@]}"; do
  var_cont=""
  eval "var_cont=\$ARGS_HELPER_OUT_FLAG_${var_name}"
  echo "ARGS_HELPER_OUT_FLAG_${var_name} => ${var_cont}"
done

for params in "${!ARGS_HELPER_OUT_VALUES[@]}"; do
  echo "ARGS_HELPER_OUT_VALUES[\"${params}\"] => ${ARGS_HELPER_OUT_VALUES[${params}]}"
done

echo "other parameters left: >${ARGS_HELPER_OUT_UNHANDLED_ARGS}<"

echo "verbose level is ${ARGS_HELPER_OUT_VERBOSE}"

echo_fatal "Fatal Error"
echo_error "Error"
echo_warning "Warning"
echo_info "Info"
echo_debug "Debug"

if [ "${ARGS_HELPER_OUT_FLAG_TEST}" = "${ARGS_HELPER_FLAG_TRUE}" ]; then
  echo "test is activated; positive check is ok"
fi

if [ "${ARGS_HELPER_OUT_FLAG_TEST}" = "${ARGS_HELPER_FLAG_FALSE}" ]; then
  echo "test is deactivated; positive check is ok"
fi

if [ "${ARGS_HELPER_OUT_FLAG_TEST}" != "${ARGS_HELPER_FLAG_FALSE}" ]; then
  echo "test is activated; negative check is ok"
fi

if [ "${ARGS_HELPER_OUT_FLAG_TEST}" != "${ARGS_HELPER_FLAG_TRUE}" ]; then
  echo "test is deactivated; negative check is ok"
fi

echo "Complete Path of the script: ${ARGS_HELPER_OUT_BASE_SCRIPT}"
echo "Base dir of the script: ${ARGS_HELPER_OUT_BASE_DIR}"
echo "filename of the script: ${ARGS_HELPER_OUT_BASE_SCRIPT_NAME}"

exit 0
