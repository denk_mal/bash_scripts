#!/usr/bin/env bash
#
# https://gitlab.com/denk_mal/bash_scripts
#

_docker_compose_control_completions()
{
  cur_word="${COMP_WORDS[COMP_CWORD]}"
#  prev_word="${COMP_WORDS[COMP_CWORD-1]}"

  case "$COMP_CWORD" in
    1)
      if [[ "${cur_word}" =~ ^\-.* ]]; then
        COMPREPLY=("--help")
      else
        # get subdir as docker-compose app name
        mapfile -t COMPREPLY < <(compgen -d "$cur_word")
      fi
      ;;
    2)
      args=$(dc-control.bash --compl_bash)
      args=${args##*\(}
      args=${args%\)*}
      args=${args//|/ }
      mapfile -t COMPREPLY < <(compgen -W "$args" "$cur_word")
      ;;
  esac
}

complete -F _docker_compose_control_completions dc-control.bash
