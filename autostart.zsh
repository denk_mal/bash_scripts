#!/usr/bin/env zsh
#
# https://gitlab.com/denk_mal/bash_scripts
#
# this will start a list of applications and tasks on a delayed base
# first entry in the list (a number!) is the delay time
# the rest ist the full command; '~' will be replaced by the user
# home dir (${HOME})
#
# the time is the diff from the start of this script. The list will be sorted
# on reading and may be therefore not in the correct sortorder. commands with
# a starttime of zero will not be started!
#
# A '%AIL%' as command starts a search for a AppImageLauncher managed image
# that starts with the command (case insensitive)
# E.g a '%AIL% keepassxc' will find the KeePassXC.AppImages and starting it.
# https://github.com/TheAssassin/AppImageLauncher
#
# A '%FLAT%' as command starts a search for an installed flatpak app
# that contains the command (case insensitive)
# E.g a '%FLAT% keepassxc' will find the org.keepassxc.KeePassXC and starting it.
#
# list example:
# 3 ~/bin/keepassxc
# 10 nextcloud
# 5 gajim
# 15 %AIL% rssguard
# 17 %FLAT% rssguard
#

#LOGFILE=$HOME/.local/log/autostart.log
#ERRLOGFILE=${HOME}/.local/log/autostart_err.log
#exec 1>> $LOGFILE
#exec 2>> $ERRLOGFILE

DATE_FORMATSTR="${DATE_FORMATSTR-%F %T: }"

# get startlist from first parameter or default startlist
AUTOSTART_LIST="${1:-${HOME}/.config/autostart.list}"

if [ ! -s "${AUTOSTART_LIST}" ]; then
  date +"${DATE_FORMATSTR}could not find a valid startlist '${AUTOSTART_LIST}'" >&2
  exit 1
fi

date +"${DATE_FORMATSTR}setup environment..."

# execute AppImage
AppImage_Basepath=""
if [ -f "${HOME}/.config/appimagelauncher.cfg" ]; then
  AppImage_Basepath=$(grep "^destination=" "${HOME}/.config/appimagelauncher.cfg"|cut -d"=" -f2)
fi

# read commands sorted by startup time
# typeset -a cmds_in=("${(fon)"$(<${AUTOSTART_LIST})"}")
typeset -a cmds_in=("${(@f)"$(<${AUTOSTART_LIST})"}")
typeset -A command_lines
cmds=(${cmds_in:#\#*})
for cmd in ${cmds}; do
  # save wait time
  wait_time=${cmd%% *}
  command_line=${cmd#* }
  # replace all '~' with users home path
  command_line="${command_line//~\//${HOME}\/}"
  command_lines[${wait_time}]=${command_line}
done

old_wait_time=0

for wait_time in ${(kon)command_lines}; do
  command_line=${command_lines[${wait_time}]}
  command_line=${command_line//\~/${HOME}}

  # avoid starting of apps with zero wait time
  if [ "${wait_time}" -eq 0 ]; then
    continue
  fi

  # pause this script with the diff time between last wait time
  # and actual wait time
  sleep $(("${wait_time}" - "${old_wait_time}"))
  old_wait_time=${wait_time}

  case ${command_line} in
    '%AIL%'*)
      cmd_params="${command_line#* }"
      command="${cmd_params%% *}"
      cmd_params="${cmd_params#* }"
      appimage_cmd=$(find "${AppImage_Basepath}" -iname "${command}*"|sort -n|tail -n1)
      if [ "${appimage_cmd}" = "" ]; then
        appimage_cmd="${command}"
      fi
      if [ "${cmd_params}" != "" ]; then
        command_line="${appimage_cmd} ${cmd_params}"
      else
        command_line="${appimage_cmd}"
      fi
      ;;
    '%FLAT%'*)
      cmd_params="${command_line#* }"
      command="${cmd_params%% *}"
      cmd_params="${cmd_params#* }"
      if [ "${command}" = "${cmd_params}" ]; then
        cmd_params=""
      fi
      appimage_cmd=$(flatpak list --app --columns=application|grep -i "${command}"|sort -n|tail -n1)
      if [ "${appimage_cmd}" = "" ]; then
        date +"${DATE_FORMATSTR}flatpak app '${command}' not found." >&2
        continue
      fi
      appimage_cmd="flatpak run ${appimage_cmd}"
      if [ "${cmd_params}" != "" ]; then
        command_line="${appimage_cmd} ${cmd_params}"
      else
        command_line="${appimage_cmd}"
      fi
      ;;
    *)
      ;;
  esac

  command="${command_line%% *}"
  cmd_params="${command_line#* }"
  # check if command has parameter(s)
  if [ "${command}" = "${cmd_params}" ]; then
    cmd_params=""
  fi
  # check if command is found (has full path)
  if [ ! -s "${command}" ]; then
    # get command with full path
    command=$(/usr/bin/which "${command}")
  fi

  # append parameters to command
  parameter_list=()
  eval 'for param in '${cmd_params}'; do parameter_list+=("${param}"); done'
  execute=( "${command}" "${parameter_list[@]}" )

  # execute command with parameters
  if [ -x "${command%% *}" ]; then
    date +"${DATE_FORMATSTR}start '${execute[*]}'..." >&2
    nohup "${execute[@]}" &>/dev/null &
  fi
done

# notify user that all delayed autostart apps are started now
date +"${DATE_FORMATSTR}All Tasks have been started." >&2
command -v notify-send &>/dev/null && \
  notify-send 'Delayed Autostart' 'All Tasks have been started.' --icon=dialog-information
