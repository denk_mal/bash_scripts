#!/usr/bin/env bash

SERVERS=(
)

# put your serverlist into .env;
# keep the order from first server to start to last server to start
[[ ! -f .env ]] || source .env

SERVERS_REV=($(echo "${SERVERS[@]} " | tac -s ' '))

FULLPATH_SCRIPT=$(realpath -e "${0}")
# BASE_DIR=${FULLPATH_SCRIPT%/*}
SCRIPT_NAME="${FULLPATH_SCRIPT##*/}"

function start() {
  date +"%F %T: ${FUNCNAME} '${1}'..."
  dc-control.bash "${1}" "${FUNCNAME}" 2>/dev/null
  date +"%F %T: '${1}' has been ${FUNCNAME}"
}

function status() {
  date +"%F %T: ${FUNCNAME} '${1}'..."
  dc-control.bash "${1}" "${FUNCNAME}" 2>/dev/null
  date +"%F %T: '${1}' has been ${FUNCNAME}"
}

function clean() {
  date +"%F %T: ${FUNCNAME} '${1}'..."
  dc-control.bash "${1}" "${FUNCNAME}" 2>/dev/null
  date +"%F %T: '${1}' has been ${FUNCNAME}"
}

function down() {
  date +"%F %T: ${FUNCNAME} '${1}'..."
  dc-control.bash "${1}" "${FUNCNAME}" 2>/dev/null
  date +"%F %T: '${1}' has been ${FUNCNAME}"
}

func_name="${1}"

if [ "${func_name}" = "" ]; then
  echo "action missing"
  exit 255
fi

case "${func_name}" in
  status|start)
    for server in "${SERVERS[@]}"; do
      ${func_name} "${server}"
    done
    ;;
  clean|down)
    for server in "${SERVERS_REV[@]}"; do
      ${func_name} "${server}"
    done
    ;;
  *)
    echo "unknown parameter '${func_name}'!"
    exit 255
esac
