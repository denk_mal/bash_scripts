#!/usr/bin/env bash
# ./export-traefik-v2-certificate.bash [DOMAIN] [-v]
# exit 0        => new certificate available; further actions could be taken
# exit 1        => no new certificate; no further actions necessary
# exit higher 1 => error occured; stop all further actions

# original source by Jack Henschel
# https://blog.cubieserver.de/2021/minimal-traefik-v2-certificate-export/

# 2024/07/ß8 denk_mal
# - add helper script for more  verbosity

# 2023/08/26 denk_mal
# - shellcheck
# - add local.env file

# some common default settings
SCRIPT_FULLPATH=$(realpath -se "${0}")
SCRIPT_BASE_DIR="${SCRIPT_FULLPATH%/*}"

# do not end on errors because this will fail if you check for exit values!
# set -e # abort on errors
# <while loop> checks for unset var!
# (was: it is ok but always check individual for better error response)
# set -u # abort on unset variables

# adjust these variables according to your setup
TRAEFIK_CERT_STORE="acme.json"
TRAEFIK_RESOLVER="dns01"
CERT_PATH="certs"

source "${SCRIPT_BASE_DIR}/helper_commons.sh"

if [ -z "${DOMAIN}" ]; then
  log_output "No domain given!"
  exit 2
fi

info_output "check sanity of certificate..."
# minor sanity checks
if [ ! -r "${TRAEFIK_CERT_STORE}" ]; then
  log_output "File '${TRAEFIK_CERT_STORE}' not readable!"
  exit 2
fi
if ! grep "\"${DOMAIN}\"" "${TRAEFIK_CERT_STORE}" > /dev/null; then
  log_output "Domain '${DOMAIN}' not found in '${TRAEFIK_CERT_STORE}'!"
  exit 2
fi

KEY_FILE="${CERT_PATH}/${DOMAIN}.key"
CERT_FILE="${CERT_PATH}/${DOMAIN}.crt"

info_output "create new temp keys files..."
# create new files with strict permissions (mktemp defaults to 600)
NEW_KEY_FILE="$(mktemp --tmpdir XXXXX.key.new)"
NEW_CERT_FILE="$(mktemp --tmpdir XXXXX.crt.new)"

# allow ssl-cert group to read certificates (for Debian systems)
# chown fniessen:users "${NEW_CERT_FILE}" "${NEW_KEY_FILE}"
chmod 640 "${NEW_CERT_FILE}" "${NEW_KEY_FILE}"

info_output "extract certificate..."
# extract certificate
jq -r ".${TRAEFIK_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN}\") | .certificate" "${TRAEFIK_CERT_STORE}" | base64 -d > "${NEW_CERT_FILE}"

info_output "extract private key..."
# extract private key
jq -r ".${TRAEFIK_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN}\") | .key" "${TRAEFIK_CERT_STORE}" | base64 -d > "${NEW_KEY_FILE}"

info_output "check if certificate has changed..."
# check if the contents (of the cert only) changed
if diff -N "${NEW_CERT_FILE}" "${CERT_FILE}" > /dev/null; then
  # certificate unchanged, delete temporary files
  log_output "Certificate ${DOMAIN} unchanged."
  rm -f "${NEW_CERT_FILE}" "${NEW_KEY_FILE}"
  exit 1
fi

info_output "update certificate and key files..."
# certificate changed, rotate files
mv "${NEW_CERT_FILE}" "${CERT_FILE}"
mv "${NEW_KEY_FILE}" "${KEY_FILE}"
log_output "Certificate ${DOMAIN} updated."

exit 0
