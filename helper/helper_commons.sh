# source this file into script-runner/helper scripts

VERBOSE="--NO--"
DATE_FORMATSTR="${DATE_FORMATSTR-%F %T: }"

# overwrite above presets in a local settingsfile
# shellcheck source=/dev/null
test -s "${SCRIPT_BASE_DIR}/local.env" && source "${SCRIPT_BASE_DIR}/local.env"

while [ "${1}" != "" ]; do
  case "${1}" in
    "-v")
      VERBOSE="--YES--"
      ;;
    "-q")
      VERBOSE="--NO--"
      ;;
    "-"*)   # supress any not handled flags!
      ;;
    *)
      DOMAIN=${1:-${DOMAIN}}  # command line args overrules local.var settings
      ;;
  esac
  shift
done

log_output() {
  date +"${DATE_FORMATSTR}${1}"
}

info_output() {
  [ "${VERBOSE}" = "--YES--" ] && date +"${DATE_FORMATSTR}${1}"
}
