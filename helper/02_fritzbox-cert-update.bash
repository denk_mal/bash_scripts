#!/usr/bin/env bash
# ./fritzbox-cert-update.bash [DOMAIN] [-v]
# exit 0        => no new certificate; no further actions necessary
# exit higher 1 => error occured; stop all further actions

# original source by wikrie
# https://gist.github.com/wikrie/f1d5747a714e0a34d0582981f7cb4cfb

# 2024/07/ß8 denk_mal
# - add helper script for more  verbosity

# 2023/08/26 denk_mal
# - shellcheck bulletproofed
# - add local.env file

# some common default settings
SCRIPT_FULLPATH=$(realpath -se "${0}")
SCRIPT_BASE_DIR="${SCRIPT_FULLPATH%/*}"

# parameters
FB_HOST="http://fritz.box"
FB_USERNAME="needed since Fritz OS 7.25"
FB_PASSWORD="fritzbox-password"
CERT_PATH="path to cert eg  /etc/letsencrypt/live/domain.tld"
CERT_PASSWORD="cert password if needed"
CERT_PASSWORD=""

KEY_FILE="${CERT_PATH}/privkey.pem"
CERT_FILE="${CERT_PATH}/fullchain.pem"

source "${SCRIPT_BASE_DIR}/helper_commons.sh"

# check if key and cert files exists
info_output "check validity of key and cert files..."
if [ ! -s "${KEY_FILE}" ]; then
  log_output "can't access keyfile '${KEY_FILE}'!"
  exit 2
fi
if [ ! -s "${CERT_FILE}" ]; then
  log_output "can't access certificate file '${CERT_FILE}'!"
  exit 2
fi

# make and secure a temporary file
TMP="$(mktemp -t XXXXXX)"
chmod 600 "${TMP}"

# login to the box and get a valid SID
info_output "login into fritzbox..."
CHALLENGE=$(wget -q -O - ${FB_HOST}/login_sid.lua | sed -e 's/^.*<Challenge>//' -e 's/<\/Challenge>.*$//')
if [ "${CHALLENGE}" = "" ]; then
  log_output "can't get challange from fritzbox '${FB_HOST}'!"
  exit 2
fi

info_output "calculate hash with challange from fritzbox..."
HASH=$(echo -n "${CHALLENGE}-${FB_PASSWORD}" | iconv -f ASCII -t UTF16LE |md5sum|awk '{print $1}')
if [ "${HASH}" = "" ]; then
  log_output "can't calculate hash with challange '${CHALLENGE}'!"
  exit 2
fi

info_output "fetch sid from fritzbox..."
SID=$(wget -q -O - "${FB_HOST}/login_sid.lua?sid=0000000000000000&username=${FB_USERNAME}&response=${CHALLENGE}-${HASH}"| sed -e 's/^.*<SID>//' -e 's/<\/SID>.*$//')
if [ "${SID}" = "0000000000000000" ]; then
  log_output "can't get sid from fritzbox '${FB_HOST}' with user '${FB_USERNAME}'!"
  exit 2
fi

# generate our upload request
BOUNDARY="---------------------------"$(date +"%Y%m%d%H%M%S")
{
echo -e "--${BOUNDARY}\r"
echo -e "Content-Disposition: form-data; name=\"sid\"\r\n\r\n${SID}\r"
echo -e "--${BOUNDARY}\r"
echo -e "Content-Disposition: form-data; name=\"BoxCertPassword\"\r\n\r\n${CERT_PASSWORD}\r"
echo -e "--${BOUNDARY}\r"
echo -e "Content-Disposition: form-data; name=\"BoxCertImportFile\"; filename=\"BoxCert.pem\"\r"
echo -e "Content-Type: application/octet-stream\r\n\r"
cat "${KEY_FILE}"
cat "${CERT_FILE}"
echo -e -n "\r\n--${BOUNDARY}--"
} >> "${TMP}"

info_output "upload cert to fritzbox..."
# upload the certificate to the box
wget -q -O - "${FB_HOST}/cgi-bin/firmwarecfg" --header="Content-type: multipart/form-data boundary=${BOUNDARY}" --post-file "${TMP}" | grep SSL

# clean up
rm -f "${TMP}"

log_output "Update of certificate on fritzbox '${FB_HOST}' ok."
exit 0
